#Pull base Image
FROM tomcat:8-jre8

#Maintainer
MAINTAINER "rajasai1602@gmail.com"

#Copy war files to image path
ADD target/webapp.war /usr/local/tomcat/webapps/

